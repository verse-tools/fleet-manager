import { Progress, Spinner } from '@nextui-org/react';

export default function Loading() {
  return (
    <div className="flex flex-col my-20 items-center">
      <Spinner label="Loading..." size="lg" />
    </div>
  );
}
