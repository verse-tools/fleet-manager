import {
  ClerkLoaded,
  ClerkLoading,
  SignInButton,
  SignOutButton,
  SignedIn,
  SignedOut,
  UserButton,
  currentUser,
} from '@clerk/nextjs';
import { faSpaceAwesome } from '@fortawesome/free-brands-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  Link,
  Navbar,
  NavbarBrand,
  NavbarContent,
  NavbarItem,
  NavbarMenu,
  NavbarMenuItem,
  NavbarMenuToggle,
  Spinner,
  User,
} from '@nextui-org/react';

export default async function Header() {
  const user = await currentUser();

  return (
    <Navbar disableAnimation isBordered>
      <NavbarContent className="sm:hidden" justify="start">
        <NavbarMenuToggle />
      </NavbarContent>

      <NavbarContent className="sm:hidden pr-3" justify="center">
        <NavbarBrand>
          <FontAwesomeIcon icon={faSpaceAwesome} />
          <p className="ml-2 font-bold text-inherit">
            <Link href="/">Fleet manager</Link>
          </p>
        </NavbarBrand>
      </NavbarContent>

      <NavbarContent className="hidden sm:flex gap-4" justify="center">
        <NavbarBrand>
          <FontAwesomeIcon icon={faSpaceAwesome} />
          <p className="ml-2 font-bold text-inherit">
            <Link href="/">Fleet manager</Link>
          </p>
        </NavbarBrand>
        <ClerkLoading>
          <Spinner color="default" size="sm" aria-label="Loading..." />
        </ClerkLoading>
        <ClerkLoaded>
          <SignedIn>
            <NavbarItem>
              <Link
                color="foreground"
                href={'/user/' + user?.username + '/hangar'}
              >
                Hangar
              </Link>
            </NavbarItem>
          </SignedIn>
        </ClerkLoaded>
        <NavbarItem>
          <Link color="foreground" href="/user">
            Users
          </Link>
        </NavbarItem>
        <NavbarItem>
          <Link color="foreground" href="/org">
            Organizations
          </Link>
        </NavbarItem>
      </NavbarContent>

      <NavbarContent className="hidden sm:flex" justify="end">
        <ClerkLoading>
          <Spinner color="default" size="sm" aria-label="Loading..." />
        </ClerkLoading>
        <ClerkLoaded>
          <SignedOut>
            <SignInButton />
          </SignedOut>
          <SignedIn>
            <UserButton
              showName
              afterSignOutUrl="/"
              userProfileMode="navigation"
              userProfileUrl="/profile"
            />
          </SignedIn>
        </ClerkLoaded>
      </NavbarContent>

      <NavbarMenu>
        <ClerkLoading>
          <Spinner color="default" size="sm" aria-label="Loading..." />
        </ClerkLoading>
        <ClerkLoaded>
          <SignedOut>
            <SignInButton>
              <Link className="w-full" size="lg">
                Sign in
              </Link>
            </SignInButton>
          </SignedOut>
          <SignedIn>
            <NavbarMenuItem key="profile">
              <Link className="w-full" href="/profile" size="lg">
                <User
                  name={user?.username}
                  description={user?.emailAddresses[0].emailAddress}
                  avatarProps={{
                    src: user?.imageUrl || '',
                  }}
                />
              </Link>
            </NavbarMenuItem>
            <NavbarMenuItem key="fleet">
              <Link
                className="w-full"
                href={'/user/' + user?.username + '/hangar'}
                size="lg"
              >
                Hangar
              </Link>
            </NavbarMenuItem>
          </SignedIn>
          <NavbarMenuItem key="users">
            <Link className="w-full" href="/user" size="lg">
              Users
            </Link>
          </NavbarMenuItem>
          <NavbarMenuItem key="organization">
            <Link className="w-full" href="/org" size="lg">
              Organizations
            </Link>
          </NavbarMenuItem>
          <SignedIn>
            <NavbarMenuItem>
              <SignOutButton />
            </NavbarMenuItem>
          </SignedIn>
        </ClerkLoaded>
      </NavbarMenu>
    </Navbar>
  );
}
