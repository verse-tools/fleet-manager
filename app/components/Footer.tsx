import { Button } from '@nextui-org/react';

export default function Footer() {
  return (
    <footer className="container mx-auto max-w-7xl pb-12 px-12">
      <Button color="primary" variant="light">
        About
      </Button>
    </footer>
  );
}
