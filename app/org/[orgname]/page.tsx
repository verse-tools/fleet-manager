interface Props {
  params: { orgname: string };
}

export default function Organization({ params: { orgname } }: Props) {
  return <div>Organization {orgname}</div>;
}
