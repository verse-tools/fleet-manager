import { SignIn } from '@clerk/nextjs';

export default function SignInPage() {
  return (
    <div className="flex flex-col items-center my-4 lg:my-40">
      <SignIn />
    </div>
  );
}
