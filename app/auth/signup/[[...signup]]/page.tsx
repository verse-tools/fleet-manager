import { SignUp } from '@clerk/nextjs';

export default function SignUpPage() {
  return (
    <div className="flex flex-col items-center my-4 lg:my-40">
      <SignUp />
    </div>
  );
}
