'use client';

import {
  Link,
  Table,
  TableBody,
  TableCell,
  TableColumn,
  TableHeader,
  TableRow,
  getKeyValue,
} from '@nextui-org/react';
import { User } from '@prisma/client';

const columns = [
  {
    key: 'username',
    label: 'Username',
  },
  {
    key: 'email',
    label: 'Email',
  },
  {
    key: 'roles',
    label: 'Roles',
  },
  {
    key: 'orgs',
    label: 'Organizations',
  },
];

interface Props {
  users: User[];
}

export default function UserTable({ users }: Props) {
  return (
    <Table aria-label="Example table with dynamic content">
      <TableHeader columns={columns}>
        {(column) => <TableColumn key={column.key}>{column.label}</TableColumn>}
      </TableHeader>
      <TableBody items={users} emptyContent="No users to display.">
        {(item) => (
          <TableRow key={item.id} href={"/user/" + item.username} className="hover:cursor-pointer">
            {(columnKey) => (
              <TableCell>{getKeyValue(item, columnKey)}</TableCell>
            )}
          </TableRow>
        )}
      </TableBody>
    </Table>
  );
}
