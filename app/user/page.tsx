import { prisma } from '@/prisma/client';
import UserTable from './components/UserTable';
import { currentUser } from '@clerk/nextjs';

export const dynamic = 'force-dynamic';

function emailMasking(email: string) {
  const parts = email.split('@');
  const localPart = parts[0].replace(/[a-zA-Z0-9]/g, 'x');
  const domainParts = parts[1].split('.', 2);
  const anonymizedDomainParts =
    domainParts[0].replace(/[a-zA-Z0-9]/g, 'x') +
    '.' +
    domainParts[1].replace(/[a-zA-Z0-9]/g, 'x');
  return localPart + '@' + anonymizedDomainParts;
}

export default async function Users() {
  const users = await prisma.user.findMany({
    where: { privateProfile: false },
    orderBy: { username: 'asc' },
  });

  const user = await currentUser();

  if (user) {
    const storedUser = await prisma.user.findUnique({
      where: { username: user!.username! },
    });
    if (storedUser) {
      users.push(storedUser);
    }
  }

  users.sort((user1, user2) => {
    if (user1.username > user2.username) {
      return 1;
    }
    if (user1.username < user2.username) {
      return -1;
    }
    return 0;
  });

  // anonymize email if user wants to mask it
  for (let user of users) {
    if (user.hideEmail) {
      user.email = emailMasking(user.email);
    }
  }

  return <UserTable users={users} />;
}
