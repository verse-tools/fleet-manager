interface Props {
  params: { username: string };
}

export default function Hangar({ params: { username } }: Props) {
  return <div>Hangar of user {username}</div>;
}
