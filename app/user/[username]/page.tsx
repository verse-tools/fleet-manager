import { prisma } from '@/prisma/client';
import { clerkClient, currentUser } from '@clerk/nextjs';
import { Card, CardFooter, Chip, Image } from '@nextui-org/react';
import { notFound } from 'next/navigation';

interface Props {
  params: { username: string };
}

export default async function User({ params: { username } }: Props) {
  const loggedUser = await currentUser();
  let storedUser = null;

  if (loggedUser?.username === username) {
    storedUser = await prisma.user.findUnique({
      where: { username: username },
    });
  } else {
    storedUser = await prisma.user.findUnique({
      where: { username: username, privateProfile: false },
    });
  }

  if (!storedUser) {
    return notFound();
  }

  const clerkUser = await clerkClient.users.getUser(storedUser.clerkId);
  
  return (
    <div className="flex flex-col items-center">
      <Card isFooterBlurred radius="lg" className="border-none">
        <Image
          alt="User's avatar"
          className="object-cover"
          height={250}
          src={clerkUser.imageUrl}
          width={250}
        />
        <CardFooter className="justify-between before:bg-white/10 border-white/20 border-1 overflow-hidden py-1 absolute before:rounded-xl rounded-large bottom-1 w-[calc(100%_-_8px)] shadow-small ml-1 z-10">
          <p className="text-tiny text-white/80">{storedUser.rsiHandle}</p>
          <Chip size="sm" variant="shadow" color="warning">
            Mercenary (RSI badge)
          </Chip>
        </CardFooter>
      </Card>
      <h2 className="text-5xl font-semibold">
        {storedUser.username}
        {!storedUser.hideEmail &&<p className="text-small text-default-400">{storedUser.email}</p>}
      </h2>
      <h2>Add orgs there (chips for roles)</h2>
      <h2>Add ships there</h2>
    </div>
  );
}
