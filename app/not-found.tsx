import lagrangePicture from '@/public/images/lagrange-picture.jpg';
import { Card, CardHeader, Link } from '@nextui-org/react';
import Image from 'next/image';

export default function NotFound() {
  return (
    <Card
      className="border-none bg-background/60 dark:bg-default-100/50 max-w-full"
      shadow="sm"
    >
      <CardHeader className="absolute z-10 top-1 flex-col items-center">
        <p className="text-tiny text-white/60 uppercase font-bold">404</p>
        <h4 className="text-white font-medium text-large sm:mt-10 lg:mt-40">
          You lost yourself in the verse...
        </h4>
        <Link href="/" className='text-small mt-5'>Back to homepage</Link>
      </CardHeader>
      <Image
        className="z-0 w-full h-full object-cover"
        alt="lagrange picture"
        src={lagrangePicture}
      />
    </Card>
  );
}
