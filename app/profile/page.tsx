import { prisma } from '@/prisma/client';
import { RedirectToSignIn, currentUser } from '@clerk/nextjs';
import ProfileForm from './ProfileForm';

export default async function Profile() {
  const user = await currentUser();
  if (!user) {
    <RedirectToSignIn />;
  }

  const storedUser = await prisma.user.findUnique({
    where: { username: user!.username! },
  });
  return (
    <div className="flex flex-col">
      <ProfileForm user={storedUser!} />
    </div>
  );
}
