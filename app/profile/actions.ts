'use server';

import { prisma } from "@/prisma/client";
import { currentUser, redirectToSignIn } from "@clerk/nextjs";
import { revalidatePath } from "next/cache";

export async function updateProfile(
    prevState: {
        message: string;
    }, formData: FormData) {
    const user = await currentUser();

    if (!user) {
        redirectToSignIn();
    }

    const rawFormData = {
        rsiHandle: formData.get('rsiHandle'),
        syncWithRsiHandle: formData.get('syncWithRsiHandle'),
        hideEmail: formData.get('hideEmail'),
        privateProfile: formData.get('privateProfile')
    };

    try {
        const update = await prisma.user.update({
            where: { username: user!.username! },
            data: { rsiHandle: rawFormData.rsiHandle?.toString(), hideEmail: rawFormData.hideEmail !== null, privateProfile: rawFormData.privateProfile !== null }
        });
    
        // let message = "Error while updating the profile, please try again...";
        if (update) {
            revalidatePath('/profile');
            return {message: "Profile updated successfully", success: true};
        }
        else {
            return {message: "Error while updating the profile, please try again...", success: true};
        }
    }
    catch (e) {
        return {message: "Error while updating the profile, please try again...", success: true};
    }
}