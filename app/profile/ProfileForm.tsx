'use client';

import { UserProfile } from '@clerk/nextjs';
import { Button, Divider, Input, Switch } from '@nextui-org/react';
import { User } from '@prisma/client';
import { useEffect } from 'react';
import { useFormState } from 'react-dom';
import { ToastOptions, toast } from 'react-toastify';
import { updateProfile } from './actions';

interface Props {
  user: User;
}

const initialState = {
  message: '',
  success: false,
};

export default function ProfileForm({ user }: Props) {
  let [state, formAction] = useFormState(updateProfile, initialState);

  useEffect(() => {
    const options: ToastOptions = {
      theme: 'dark',
      type: state.success ? 'success' : 'error',
    };
    if (state.message !== '') {
      toast(state.message);
    }
  }, [state]);

  return (
    <form action={formAction}>
      <div className="flex flex-col lg:ml-10">
        <h2 className="text-2xl font-medium">RSI information</h2>
        <p className="text-small text-default-400">
          Add your RSI handle. This helps others to find you ingame as well as
          adding more information to your public profile if you enable syncing.
        </p>
        <div className="flex flex-col lg:flex-row mt-5">
          <Input
            id="rsiHandle"
            name="rsiHandle"
            type="text"
            label="RSI handle"
            variant="bordered"
            className="max-w-xs"
            defaultValue={user?.rsiHandle || ''}
          />
          <Switch
            id="syncWithRsiHandle"
            name="syncWithRsiHandle"
            size="sm"
            className="mt-5 lg:ml-5"
          >
            <span className="text-lg">Sync with with your RSI profile</span>
            <p className="text-small text-default-400">
              Retrieve information from your public RSI profile (language,
              location, organizations...)
            </p>
          </Switch>
        </div>
      </div>
      <Divider className="my-4" />
      <div className="flex flex-col lg:ml-10">
        <h2 className="text-2xl font-medium">Privacy settings</h2>
        <p className="text-small text-default-400">
          Choose our your profile information is displayed on public pages.
        </p>
        <Switch
          id="hideEmail"
          name="hideEmail"
          size="sm"
          defaultSelected
          className="mt-5"
        >
          <span className="text-lg">Hide email</span>
          <p className="text-small text-default-400">
            Do not show you email on profile.
          </p>
        </Switch>
        <Switch
          id="privateProfile"
          name="privateProfile"
          size="sm"
          className="mt-5"
        >
          <span className="text-lg">Private profile</span>
          <p className="text-small text-default-400">
            Make your profile completely invisible to others.
          </p>
        </Switch>
      </div>
      <Divider className="my-4" />
      <div className="flex flex-col lg:ml-10">
        <h2 className="text-2xl font-medium">
          Authentification and personal information
        </h2>
        <p className="text-small text-default-400">
          Manage your Fleet Manager account, set your username, social account
          and more.
        </p>
        <div className="flex flex-col items-center mt-5">
          <UserProfile />
        </div>
      </div>
      <Divider className="my-4" />
      <div className="flex flex-row-reverse">
        <Button type="submit" color="danger">
          Update profile
        </Button>
      </div>
    </form>
  );
}
