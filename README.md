# Getting Started

## Install dependencies

```bash
npm install
```

## Prepare the database

Choose between one of the following methods.

> Warning: Don't forget the `?pgbouncer=true`, otherwise Prisma will randomly fail with the error "prepared statement \'s#\' does not exist". See https://supabase.com/partners/integrations/prisma for more information.


### Docker

1. Init the container
```bash
docker run --name postgresql -e POSTGRES_PASSWORD=yourdbpassword -e POSTGRES_DB=fleetmanager -p 5432:5432 -v postgres-data:/var/lib/postgresql/data -d postgres:alpine
```
2. Add variable to your `.env.development.local`
```
DATABASE_URL="posgres://postgres:yourdbpassword@localhost:5432/fleetmanager"
```

### Supabase

1. Create an account on http://supabase.com
2. Create a project
3. Create a postgresql database
4. Get your connection string from `Project settings > Database > Connection String`
5. Copy / paste the value in `.env.development.local` for `DATABASE_URL`
6. Add `?pgbouncer=true&connection_limit=1` at the end
7. Go back to the connection string, select mode `Session`, we need direct connection for prisma migrations
8. Copy / paste the value in `.env.development.local` for `DIRECT_URL`

### Vercel

1. Create an account on https://vercel.com/
2. Create a project
3. Create a postgresql database (under storage, and link it it to the project)
4. Get your connection string from `Storage > Your database name > .env.local`
5. Copy / paste the value of `POSTGRES_PRISMA_URL` in `.env.development.local` as `DATABASE_URL`
6. Go back to the connection string, untick the `Use connection pooling`, we need direct connection for prisma migrations
6. Copy / paste the value of `POSTGRES_URL_NON_POOLING` in `.env.development.local` as `DIRECT_URL`

## Init database

### Note on Prisma commands

`.env` is not commited and is used for global not sensitive app configuration.

We use the NextJS standard `.env*.local`, which are git ignored. 

Prisma uses `.env` file. So to execute, instead of directly use `npx prisma`, use `npm run prisma <prisma commmand>` (script already added to `package.json`).

### Generate the prisma client

```bash
npm run prisma generate
```

### Create tables

To sync the migrations files with a freshly installed PostgreSQL server, execute the following:

```bash
npm run prisma migrate dev
```

## Authentification with Clerk

We use Clerk to manage authentification. A development app is needed to make the authentification work properly.

1. Create an account on https://clerk.com
2. Create an application
3. In `User & Authentication > Email, Phone, Username`, enable:
   - Email address
   - Username
4. In `User & Authentication > Social Connections`, enable:
   - At least Google or Discord
5. Copy the `CLERK_SECRET_KEY` and `NEXT_PUBLIC_CLERK_PUBLISHABLE_KEY` to `.env.development.local`

### Webhook

We store some additional user information in postgres. To get notified of any user creation/update/deletion, we need a webhook

1. Go to `Webhook` in the Clerk app
2. Generate an url with svix play for now
3. Select `user` in the filter events

To make Clerk webhook work, the local API has to be exposed to internet.

The best approach is to use ngrok.

1. Create a account on ngrok
2. Follow the setup to install the client
3. Create a domain (one free allowed)
4. Run the command to start ngrok, don't forget to specify 3000 instead of 80 for the port:
```
ngrok http --domain=my-static-domain.ngrok-free.app 3000
```
5. Modify the previously created webhook with this url, ex: `https://my-static-domain.ngrok-free.app/api/webhook/user` (note the `/api/webhook/user` which is the local api which handles the user sync) 
6. Copy the webhook to `.env.development.local` for `WEBHOOK_USER_SECRET`

## Start the development server

Now that everything if configured, you can start the local server

```
npm run dev
```

Navigate to http://localhost:3000 and test if you can login. Try to modify your username and check that the modification has been made in the local database (plus, a webhook trace in the console)
